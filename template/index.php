<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>抽奖管理</title>

    <!-- Bootstrap -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->
    <style>
        .center-vertical {
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            position: fixed;
            top: 50%;
            left: 50%;
        }
        .special-box{
            position: fixed;
            top: 10%;
            right: 1%;
            width: 100px;
            height: 100px;
        }
    </style>
</head>
<body style="background: url(../images/index-bg.jpg)">
    <a href="index.php?type=0">
        <div class="special-box">

        </div>
    </a>
    <div class="container center-vertical">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="index.php?type=1"><img class="img-responsive" src="../images/1.png"></a>
            </div>
            <div class="col-md-4">
                <a href="index.php?type=2"><img class="img-responsive" src="../images/2.png"></a>
            </div>
            <div class="col-md-4">
                <a href="index.php?type=3"><img class="img-responsive" src="../images/3.png"></a>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="index.php?type=4"><img class="img-responsive" src="../images/4.png"></a>
            </div>
            <div class="col-md-4">
                <a href="index.php?type=5"><img class="img-responsive" src="../images/5.png"></a>
            </div>
            <div class="col-md-4">
                <a href="index.php?type=6"><img class="img-responsive" src="../images/6.png"></a>
            </div>
        </div>

    </div><!-- /.container -->
    <audio id="myaudio" src="../images/index.mp3" autoplay="autoplay" controls="controls" loop="true" hidden="true" ></audio>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../bootstrap/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../bootstrap/main.js"></script>
</body>
</html>